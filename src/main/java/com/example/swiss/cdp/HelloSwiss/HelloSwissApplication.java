package com.example.swiss.cdp.HelloSwiss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloSwissApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloSwissApplication.class, args);
    }
}
